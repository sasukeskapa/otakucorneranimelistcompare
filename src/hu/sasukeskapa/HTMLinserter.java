package hu.sasukeskapa;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by sasukeskapa on 2015.06.27. yay
 */
public class HTMLinserter {
    private static ArrayList<String> framePart1;
    private static ArrayList<String> framePart2;
    private static ArrayList<String> oldTable;

    public static void HTMLinsertion(String newTable, String file) {
        boolean beforeTable = true;
        boolean afterTable = false;
        System.out.println("HTML inserting into: \'" + file + "\'");

        framePart1 = new ArrayList<>();
        framePart2 = new ArrayList<>();
        oldTable = new ArrayList<>();
        StringBuilder sBuild = new StringBuilder();

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            for (String line; (line = br.readLine()) != null; ) {
                if (line.matches(".*<!-- the table part starts here -->.*")) {
                    beforeTable = false;
                    framePart1.add(line);
                } else if (line.matches(".*<!-- the table part ends here -->.*")) {
                    afterTable = true;
                    framePart2.add(line);
                } else if (!beforeTable && !afterTable) {
                    oldTable.add(line);
                } else if (beforeTable && !afterTable) {
                    framePart1.add(line);
                } else if (!beforeTable) {
                    framePart2.add(line);
                } else {
                    System.out.println("Dafuq o_O");
                    return;
                }
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        for (int i = 0; i < framePart1.size(); i++) {
            sBuild.append(framePart1.get(i) + "\n");
        }
        sBuild.append("\n" + newTable + "\n");
        for (int i = 0; i < framePart2.size(); i++) {
            sBuild.append(framePart2.get(i) + "\n");
        }
        String newHTML = sBuild.toString();
        //System.out.println(newHTML);
        try {
            PrintWriter printer = new PrintWriter(new File(file));
            printer.print(newHTML);
            printer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}

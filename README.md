#An anime compare site creator software for the group: Otaku Corner.

#How to run it?
"git clone" the project or download the zip and load the src folder into your preferred IDE, generate a jar from it and run it like:  "*java -jar OtakuCornerAnimeListCompare_jar.jar -h*".
And this will print you the help information to run it.

The program requires the "data_source_txts" folder next to it, filled with "username_mal_xml.txt"-s for the users you'd like to compare. These files should contain the xml from: 

* "myanimelist.net/malappinfo.php?u=username&status=all&type=anime"

It will create 2 txt files:

* CLIoutput.txt
* HTMLoutput.txt

These will contain the generated HTML code and a simple .txt output of the ~same data.

With "-f" you can tell it where all of these files are located. 

With "-o" you can tell it in which .html file would you like to embed the HTMLoutput.txt.

(It will put it between the lines:

* "<!-- the table part starts here -->"
* "<!-- the table part ends here -->")

For examples run the .jar with "-h".

#Example implementation:
http://sasukeskapa.com/otakucornercompare.html

#How to contribute?
Make an issue with things you think would benefit the project.

#Contributors:
* Daniel Dande (programmer)
* Adam Berta (artist for the logo)

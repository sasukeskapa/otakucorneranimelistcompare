package hu.sasukeskapa;

import java.util.ArrayList;

/**
 * Created by sasukeskapa on 2015.06.23. yay
 */
public class PersonsData {
    private String name;
    private String daysSpendWatching;
    private ArrayList<AnimeData> currentlyWatching;
    private ArrayList<AnimeData> completed;
    private ArrayList<AnimeData> onHold;
    private ArrayList<AnimeData> planToWatch;
    private ArrayList<AnimeData> dropped;
    private boolean fresh;

    public void setFresh() {
        fresh = true;
    }

    public boolean isFresh() {
        return fresh;
    }

    public PersonsData(String name) {
        currentlyWatching = new ArrayList<>();
        completed = new ArrayList<>();
        onHold = new ArrayList<>();
        planToWatch = new ArrayList<>();
        dropped = new ArrayList<>();
        this.name = name;
        daysSpendWatching = "0";
        fresh = false;

    }

    /**
     * @param animeName name of the anime
     * @return the AnimeData if found
     */
    public AnimeData getAnime(String animeName) {
        for (int i = 0; i < currentlyWatching.size(); i++) {
            if (animeName.equals(currentlyWatching.get(i).getTitle())) {
                return currentlyWatching.get(i);
            }
        }
        for (int i = 0; i < completed.size(); i++) {
            if (animeName.equals(completed.get(i).getTitle())) {
                return completed.get(i);
            }
        }
        for (int i = 0; i < onHold.size(); i++) {
            if (animeName.equals(onHold.get(i).getTitle())) {
                return onHold.get(i);
            }
        }
        for (int i = 0; i < planToWatch.size(); i++) {
            if (animeName.equals(planToWatch.get(i).getTitle())) {
                return planToWatch.get(i);
            }
        }
        for (int i = 0; i < dropped.size(); i++) {
            if (animeName.equals(dropped.get(i).getTitle())) {
                return dropped.get(i);
            }
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder sBuild = new StringBuilder();
        sBuild.append("PersonsData{ name=\'" + name + "\'\n");
        sBuild.append("currentlyWatching list size=" + currentlyWatching.size() + "\n");
        sBuild.append("completed list size=" + completed.size() + "\n");
        sBuild.append("onHold list size=" + onHold.size() + "\n");
        sBuild.append("planToWatch list size=" + planToWatch.size() + "\n");
        sBuild.append("dropped list size=" + dropped.size() + "\n");
        sBuild.append("    currentlyWatching list:\n");
        for (int i = 0; i < currentlyWatching.size(); i++) {
            sBuild.append("\t\t" + currentlyWatching.get(i).toString() + "\n");
        }
        sBuild.append("    completed list:\n");
        for (int i = 0; i < completed.size(); i++) {
            sBuild.append("\t\t" + completed.get(i).toString() + "\n");
        }
        sBuild.append("    onHold list:\n");
        for (int i = 0; i < onHold.size(); i++) {
            sBuild.append("\t\t" + onHold.get(i).toString() + "\n");
        }
        sBuild.append("    planToWatch list:\n");
        for (int i = 0; i < planToWatch.size(); i++) {
            sBuild.append("\t\t" + planToWatch.get(i).toString() + "\n");
        }
        sBuild.append("    dropped list:\n");
        for (int i = 0; i < dropped.size(); i++) {
            sBuild.append("\t\t" + dropped.get(i).toString() + "\n");
        }
        sBuild.append("}\n");
        return sBuild.toString();
    }

    public String getName() {
        return name;
    }

    public ArrayList<AnimeData> getCurrentlyWatching() {
        return currentlyWatching;
    }

    public ArrayList<AnimeData> getCompleted() {
        return completed;
    }

    public ArrayList<AnimeData> getOnHold() {
        return onHold;
    }

    public ArrayList<AnimeData> getPlanToWatch() {
        return planToWatch;
    }

    public ArrayList<AnimeData> getDropped() {
        return dropped;
    }

    public void addCurrentlyWatching(AnimeData anime) {
        currentlyWatching.add(anime);
    }

    public void addCompleted(AnimeData anime) {
        completed.add(anime);
    }

    public void addOnHold(AnimeData anime) {
        onHold.add(anime);
    }

    public void addPlanToWatch(AnimeData anime) {
        planToWatch.add(anime);
    }

    public void addDropped(AnimeData anime) {
        dropped.add(anime);
    }

    public String getDaysSpendWatching() {
        return daysSpendWatching;
    }

    public void setDaysSpendWatching(String daysSpendWatching) {
        this.daysSpendWatching = daysSpendWatching;
    }
}

package hu.sasukeskapa;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by sasukeskapa on 2015.06.23. yay
 */
public class DataGet {
    private static final String link1 = "https://myanimelist.net/malappinfo.php?u=";
    private static final String link2 = "&status=all&type=anime";
    private static final String animeLink = "http://myanimelist.net/anime/";

    private static final DocumentBuilderFactory DOCUMENT_BUILDER_FACTORY = DocumentBuilderFactory.newInstance();
    private static final XPathFactory XPATH_FACTORY = XPathFactory.newInstance();

    public PersonsData getDataForParam(String name, String untilAPIaccessFileLocation) {
        // String link = link1 + name + link2;
        PersonsData personsData = null;
        try {
            personsData = new PersonsData(name);
            DocumentBuilder documentBuilder = DOCUMENT_BUILDER_FACTORY.newDocumentBuilder();
            XPath xPathEvaluator = XPATH_FACTORY.newXPath();
            String file = untilAPIaccessFileLocation + name + "_mal_xml.txt";
            //try downloading fresh data
            //todo: fine tune the time and chance values
            String xml;
            for (int i = 0; i < 3; i++) {           //how many times we try to get the data equals i
                xml = downloadXMLData(name, file);
                if (!"fail".equals(xml)) {
                    personsData.setFresh();
                    i = 1000000;                    //a value much higher than any reasonable value for i
                } else {
                    Thread.sleep(200);            //how much we wait between tries
                }
            }
            //file preparation -> delete \n before xml header
            BufferedReader br = new BufferedReader(new FileReader(file));
            StringBuilder sBuildTemp = new StringBuilder();
            boolean modified = false;
            for (String line; (line = br.readLine()) != null; ) {
                if (line.matches("[ \t\n]*")) {
                    modified = true;
                } else {
                    sBuildTemp.append(line);
                    sBuildTemp.append("\n");
                }
            }
            br.close();
            if (modified) {
                System.out.println("  !!! " + file + " was modified, as it had empty lines!!!");
                PrintWriter pw = new PrintWriter(file);
                pw.print(sBuildTemp.toString());
                pw.close();
            }
            //real part
            System.out.println(file);
            final Document document = documentBuilder.parse(new File(file));
            XPathExpression nameExpr = xPathEvaluator.compile("myanimelist/myinfo/user_days_spent_watching");
            Node nodeDays = (Node) nameExpr.evaluate(document, XPathConstants.NODE);
            personsData.setDaysSpendWatching(nodeDays.getTextContent());
            nameExpr = xPathEvaluator.compile("myanimelist/anime");
            NodeList nodes = (NodeList) nameExpr.evaluate(document, XPathConstants.NODESET);
            for (int i = 0; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);
                NodeList animeNodes = node.getChildNodes();
                AnimeData tempAnimeData = new AnimeData();
                for (int j = 0; j < animeNodes.getLength(); j++) {
                    Node animeNode = animeNodes.item(j);
                    if ("series_animedb_id".equals(animeNode.getNodeName())) {
                        tempAnimeData.setLink(animeLink + animeNode.getTextContent());
                    } else if ("series_title".equals(animeNode.getNodeName())) {
                        tempAnimeData.setTitle(animeNode.getTextContent());
                    } else if ("my_tags".equals(animeNode.getNodeName())) {
                        tempAnimeData.setTag(animeNode.getTextContent());
                    } else if ("my_score".equals(animeNode.getNodeName())) {
                        tempAnimeData.setScore(animeNode.getTextContent());
                    } else if ("my_status".equals(animeNode.getNodeName())) {
                        //Watching: 1   Completed: 2    On-hold: 3  Dropped: 4  Plan to Watch: 6
                        tempAnimeData.setStatus(animeNode.getTextContent());
                    } else if ("series_end".equals(animeNode.getNodeName())) {
                        if ("0000-00-00".equals(animeNode.getTextContent()))
                            tempAnimeData.setEndDate("");
                        else
                            tempAnimeData.setEndDate(animeNode.getTextContent());
                    } else if ("series_start".equals(animeNode.getNodeName())) {
                        if ("0000-00-00".equals(animeNode.getTextContent()))
                            tempAnimeData.setStartDate("");
                        else
                            tempAnimeData.setStartDate(animeNode.getTextContent());
                    }
                }
                switch (tempAnimeData.getStatus()) {
                    case "1":     //Watching: 1
                        tempAnimeData.setStatus("watching");
                        personsData.addCurrentlyWatching(tempAnimeData);
                        break;
                    case "2":     //Completed: 2
                        tempAnimeData.setStatus("completed");
                        personsData.addCompleted(tempAnimeData);
                        break;
                    case "3":     //On-hold: 3
                        tempAnimeData.setStatus("on hold");
                        personsData.addOnHold(tempAnimeData);
                        break;
                    case "4":     //Dropped: 4
                        tempAnimeData.setStatus("drop");
                        personsData.addDropped(tempAnimeData);
                        break;
                    case "6":     //Plan to Watch: 6
                        tempAnimeData.setStatus("plan to watch");
                        personsData.addPlanToWatch(tempAnimeData);
                        break;
                    default:    //WTF: ?    //and unknown types
                        //we don't give a shit about unknown status anime :D
                }
            }
        } catch (IOException e) {
            System.out.println(name + "\'s file was not found.");
            //e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return personsData;
    }

    /**
     * @param in InputStream
     * @return String (the InputStream in String form)
     * @throws IOException
     */
    private String fromStream(InputStream in) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder out = new StringBuilder();
        String newLine = System.getProperty("line.separator");
        String line;
        while ((line = reader.readLine()) != null) {
            out.append(line);
            out.append(newLine);
        }
        return out.toString();
    }

    private String downloadXMLData(String userName, String file) {
        String result = "fail";
        try {
            String link = link1 + userName + link2;
            System.out.println("Visited this site for data: " + link);     //to see which links it visits
            URL url = new URL(link);
            URLConnection urlc = url.openConnection();
            urlc.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            urlc.setRequestProperty("Accept-Language", "en");
            // to get this crap go here: http://www.whatsmyuseragent.com/
            // urlc.setRequestProperty("User-Agent", "Ubuntu Chromium/43.0.2357.130 Chrome/43.0.2357.130");
            String downloadedData = fromStream(urlc.getInputStream());
            if (downloadedData.startsWith("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<myanimelist><myinfo><user_id>"))
                result = downloadedData;
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!"fail".equals(result)) {
            try {
                PrintWriter pw = new PrintWriter(file);
                pw.print(result);
                pw.close();
                System.out.println(userName + "\'s data was updated successfully!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("  !!! " + userName + "\'s data failed to update :(");
        }
        return result;
    }
}

package hu.sasukeskapa;

import java.io.File;

/**
 * Created by sasukeskapa on 2017.04.26..
 */
public class DataBaseBuilder implements Runnable {
    private DataBase db;
    private String untilAPIaccessFileLocation;
    private String userName;
    private boolean verbose;

    public DataBaseBuilder(DataBase db, String untilAPIaccessFileLocation, String userName, boolean verbose) {
        this.db = db;
        this.untilAPIaccessFileLocation = untilAPIaccessFileLocation;
        this.userName = userName;
        this.verbose = verbose;
    }

    @Override
    public void run() {
        DataGet dataGet = new DataGet();
        System.out.println("Data gotten from:");
        PersonsData userData = dataGet.getDataForParam(userName, untilAPIaccessFileLocation + "data_source_txts" + File.separator);
        db.addToPersonsDatas(userData);
        if (verbose)
            for (String line : userData.toString().split(System.getProperty("line.separator")))
                System.out.println(line);
    }
}

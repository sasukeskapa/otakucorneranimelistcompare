package hu.sasukeskapa;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeSet;

/**
 * Created by sasukeskapa on 2015.06.27. yay
 */
public class SiteGenerator {
    private static ArrayList<HashMap<String, String>> linesCLI;
    private static ArrayList<HashMap<String, String>> linesHTML;
    private static String date;

    private static void generateTableData(DataBase db, TreeSet<String> currentSet, boolean CLI) {
        date = new Date().toString();
        if (CLI)
            linesCLI = new ArrayList<>();
        else
            linesHTML = new ArrayList<>();
        for (String animeName : currentSet) {
            HashMap<String, String> line = new HashMap<>();
            line.put("Title", animeName);
            for (int i = 0; i < db.getPersonsDatas().size(); i++) {
                String userNameTemp = db.getPersonsDatas().get(i).getName();
                AnimeData animeDataTemp = db.getPersonsDatas().get(i).getAnime(animeName);
                if (animeDataTemp == null) {
                    line.put(userNameTemp + "_Score", "-");
                    line.put(userNameTemp + "_Score-State", "drop");
                    line.put(userNameTemp + "_Tag", "");
                    line.put(userNameTemp + "_State", "not on list");
                } else {
                    line.put("Link", animeDataTemp.getLink());
                    if ("0".equals(animeDataTemp.getScore())) {
                        line.put(userNameTemp + "_Score", "-");
                        line.put(userNameTemp + "_Score-State", " x-" + animeDataTemp.getStatus().substring(0, 1));
                    } else if ("10".equals(animeDataTemp.getScore())) {
                        line.put(userNameTemp + "_Score", animeDataTemp.getScore());
                        line.put(userNameTemp + "_Score-State", animeDataTemp.getScore() + "-" + animeDataTemp.getStatus().substring(0, 1));
                    } else {
                        line.put(userNameTemp + "_Score", animeDataTemp.getScore());
                        line.put(userNameTemp + "_Score-State", " " + animeDataTemp.getScore() + "-" + animeDataTemp.getStatus().substring(0, 1));
                    }
                    // make watching scores UpperCase
                    if (line.get(userNameTemp + "_Score-State").endsWith("w")) {
                        line.put(userNameTemp + "_Score-State", line.get(userNameTemp + "_Score-State").toUpperCase());
                    }
                    line.put(userNameTemp + "_Tag", animeDataTemp.getTag());
                    line.put(userNameTemp + "_State", animeDataTemp.getStatus());
                }
            }
            if (CLI)
                linesCLI.add(line);
            else
                linesHTML.add(line);
        }
    }

    public static String getCLITable(DataBase db) {
        StringBuilder sBuild = new StringBuilder();
        sBuild.append("Currently airing and someone watches it list:  " + db.getAnyoneWatchesCurrent().size() + "\n");
        sBuild.append(createCLITable(db, db.getAnyoneWatchesCurrent()));
        TreeSet<String> anyoneWatchesMinusCurrentlyAiring = new TreeSet<>(db.getAnyoneWatches());
        anyoneWatchesMinusCurrentlyAiring.removeAll(db.getAnyoneWatchesCurrent());
        sBuild.append("\nNot currently airing, but someone watches it list: " + anyoneWatchesMinusCurrentlyAiring.size() + "\n");
        sBuild.append(createCLITable(db, anyoneWatchesMinusCurrentlyAiring));
        sBuild.append("\nThese are on someone\'s on hold list: " + db.getAnyoneOnHold().size() + "\n");
        sBuild.append(createCLITable(db, db.getAnyoneOnHold()));
        sBuild.append("\nThese are on someone\'s plan to watch list: " + db.getAnyonePlanToWatch().size() + "\n");
        sBuild.append(createCLITable(db, db.getAnyonePlanToWatch()));
        sBuild.append("\nThese are on someone\'s dropped list: " + db.getAnyoneDrop().size() + "\n");
        sBuild.append(createCLITable(db, db.getAnyoneDrop()));
        sBuild.append("\nThese are on someone\'s completed list: " + db.getAnyoneCompleted().size() + "\n");
        sBuild.append(createCLITable(db, db.getAnyoneCompleted()));
        sBuild.append("\nThese are on everyone\'s lists: " + db.getEveryoneHas().size() + "\n");
        sBuild.append(createCLITable(db, db.getEveryoneHas()));
        return sBuild.toString();
    }

    public static String getHTMLTable(DataBase db) {
        String onSiteLink = "<A HREF=\"#currently_watching_airing\">Watching (airing)</A>, <A HREF=\"#currently_watching_aired\">Watching (already aired)</A>,"
                + " <A HREF=\"#onhold\">On Hold</A>, <A HREF=\"#plan_to_watch\">Plan to Watch</A>, <A HREF=\"#dropped\">Dropped</A>,"
                + " <A HREF=\"#completed\">Completed</A>, <A HREF=\"#shared\">Shared</A></p>\n";
        StringBuilder sBuild = new StringBuilder();
        sBuild.append("<A NAME=\"currently_watching_airing\"></A>");
        sBuild.append("<p class=\"justify\">Currently airing and someone watches it list: " + db.getAnyoneWatchesCurrent().size() + "</p>\n");
        sBuild.append(createHTMLTable(db, db.getAnyoneWatchesCurrent()));
        TreeSet<String> anyoneWatchesMinusCurrentlyAiring = new TreeSet<>(db.getAnyoneWatches());
        anyoneWatchesMinusCurrentlyAiring.removeAll(db.getAnyoneWatchesCurrent());
        sBuild.append("<p>Updated at: \'" + date + "\'</p>\n");
        sBuild.append("\n<p>&nbsp;</p>\n<p><A NAME=\"currently_watching_aired\"></A>" + onSiteLink);
        sBuild.append("\n<p class=\"justify\">Not currently airing, but someone watches it list: " + anyoneWatchesMinusCurrentlyAiring.size() + "</p>\n");
        sBuild.append(createHTMLTable(db, anyoneWatchesMinusCurrentlyAiring));
        sBuild.append("\n<p>&nbsp;</p>\n<p><A NAME=\"onhold\"></A>" + onSiteLink);
        sBuild.append("\n<p class=\"justify\">These are on someone\'s on hold list: " + db.getAnyoneOnHold().size() + "</p>\n");
        sBuild.append(createHTMLTable(db, db.getAnyoneOnHold()));
        sBuild.append("\n<p>&nbsp;</p>\n<p><A NAME=\"plan_to_watch\"></A>" + onSiteLink);
        sBuild.append("\n<p class=\"justify\">These are on someone\'s plan to watch list: " + db.getAnyonePlanToWatch().size() + "</p>\n");
        sBuild.append(createHTMLTable(db, db.getAnyonePlanToWatch()));
        sBuild.append("\n<p>&nbsp;</p>\n<p><A NAME=\"dropped\"></A>" + onSiteLink);
        sBuild.append("\n<p class=\"justify\">These are on someone\'s dropped list: " + db.getAnyoneDrop().size() + "</p>\n");
        sBuild.append(createHTMLTable(db, db.getAnyoneDrop()));
        sBuild.append("\n<p>&nbsp;</p>\n<p><A NAME=\"completed\"></A>" + onSiteLink);
        sBuild.append("\n<p class=\"justify\">These are on someone\'s completed list: " + db.getAnyoneCompleted().size() + "</p>\n");
        sBuild.append(createHTMLTable(db, db.getAnyoneCompleted()));
        sBuild.append("\n<p>&nbsp;</p>\n<p><A NAME=\"shared\"></A>" + onSiteLink);
        sBuild.append("\n<p class=\"justify\">These are on everyone\'s lists: " + db.getEveryoneHas().size() + "</p>\n");
        sBuild.append(createHTMLTable(db, db.getEveryoneHas()));
        sBuild.append("<p>Updated at: \'" + date + "\'</p>\n");
        return sBuild.toString();
    }

    private static String createCLITable(DataBase db, TreeSet<String> currentSet) {
        generateTableData(db, currentSet, true);
        StringBuilder sBuildCLI = new StringBuilder();
        sBuildCLI.append("Currently Watching Comparisons:\n");
        for (int i = 0; i < linesCLI.size(); i++) {
            HashMap<String, String> line = linesCLI.get(i);
            //print anime Title and Link
            sBuildCLI.append(line.get("Title"));
            for (int j = line.get("Title").length(); j < 76; j++) {
                sBuildCLI.append(" ");
            }
            sBuildCLI.append("  link: " + line.get("Link") + "\n");
            //print username and score
            for (int j = 0; j < db.getUserNameSet().size(); j++) {
                String name = db.getUserNameSet().descendingSet().toArray()[j].toString();
                sBuildCLI.append("  " + name + "\'s score:   " + line.get(name + "_Score-State") + "           ");
            }
            sBuildCLI.append("\n");
            //tag line
            for (int j = 0; j < db.getUserNameSet().size(); j++) {
                String name = db.getUserNameSet().descendingSet().toArray()[j].toString();
                if ("".equals(line.get(name + "_Tag")))
                    sBuildCLI.append("  " + name + "\'s tags: \'\'                ");
                else
                    sBuildCLI.append("  " + name + "\'s tags: \'" + line.get(name + "_Tag") + "\'");
            }
            sBuildCLI.append("\nUpdated at: \'" + date + "\'");
            sBuildCLI.append("\n\n");
        }
        return sBuildCLI.toString();
    }

    private static String createHTMLTable(DataBase db, TreeSet<String> currentSet) {
        generateTableData(db, currentSet, false);
        StringBuilder sBuildHTML = new StringBuilder();
        sBuildHTML.append("<table class=\"anime\">\n");

        //first row
        sBuildHTML.append("  <tr onMouseOver=\"this.className='highlight'\" onMouseOut=\"this.className='normal'\">\n    <td class=\"anime1\">Anime titles, links: (The scores have your tags in tooltip form!!!)</td>\n");
        //print usernames in the first row
        for (int j = 0; j < db.getUserNameSet().size(); j++) {
            String name = db.getUserNameSet().descendingSet().toArray()[j].toString();
            //<a href="http://myanimelist.net/profile/sasukeskapa">sasukeskapa</a>
            String powerLevel = "";
            for (int i = 0; i < db.getPersonsDatas().size(); i++) {
                if (name.equals(db.getPersonsDatas().get(i).getName()))
                    powerLevel = String.valueOf(Double.valueOf(Double.valueOf(db.getPersonsDatas().get(i).getDaysSpendWatching()) * 24).intValue());
            }
            sBuildHTML.append("    <td class=\"anime2\"><a href=\"http://myanimelist.net/profile/" + name + "\" title=\"Power Level: " + powerLevel + "\">" + name + " </a></td>\n");
        }
        sBuildHTML.append("  </tr>\n");

        //consecutive dynamic rows
        for (int i = 0; i < linesHTML.size(); i++) {
            HashMap<String, String> line = linesHTML.get(i);
            //first column
            //line.get("Title")     line.get("Link");
            sBuildHTML.append("  <tr onMouseOver=\"this.className='highlight'\" onMouseOut=\"this.className='normal'\">\n");
            sBuildHTML.append("    <td class=\"anime1\"><a href=\"" + line.get("Link") + "\">" + line.get("Title") + "</a></td>\n");
            //scores and tooltips / users
            sBuildHTML.append("  ");
            for (int j = 0; j < db.getUserNameSet().size(); j++) {
                String name = db.getUserNameSet().descendingSet().toArray()[j].toString();
                sBuildHTML.append("  <td class=\"anime2");
                sBuildHTML.append(line.get(name + "_State").replace(" ", ""));
                sBuildHTML.append("\"><div title=\"" + line.get(name + "_Tag") + "\">");
                if (!"".equals(line.get(name + "_Tag")))
                    sBuildHTML.append("<div style=\"text-decoration: underline;\">");
                if (!"".equals(line.get(name + "_Tag")))
                    if (line.get(name + "_Tag").matches("(.*[Bb][Ee][Ss][Tt].*)|(.*[Aa][Oo][Tt][Ss].*)"))
                        sBuildHTML.append("<b style=\"background-color: #ffd700; color: black;\">");
                    else
                        sBuildHTML.append("<b>");
                sBuildHTML.append(line.get(name + "_Score"));
                if (line.get(name + "_Tag").matches("(.*[Bb][Ee][Ss][Tt].*)|(.*[Aa][Oo][Tt][Ss].*)"))
                    sBuildHTML.append("!");
                if (!"".equals(line.get(name + "_Tag")))
                    sBuildHTML.append("</b>");
                if (!"".equals(line.get(name + "_Tag")))
                    sBuildHTML.append("</div>");
                sBuildHTML.append("</div></td>");
            }
            sBuildHTML.append("\n  </tr>\n");
        }

        //closing tag for the table at the end
        sBuildHTML.append("</table>\n");
        //color code example table
        sBuildHTML.append("<p>Color codes:</p>");
        sBuildHTML.append("<table style=\"width: 350px; vertical-align: center; margin-left: auto; margin-right: auto\"><tr>   ");
        sBuildHTML.append("<td class=\"anime2watching\">watching<td>   ");
        sBuildHTML.append("<td class=\"anime2completed\">completed<td>   ");
        sBuildHTML.append("<td class=\"anime2onhold\">on_hold<td>   ");
        sBuildHTML.append("<td class=\"anime2plantowatch\">plan_to_watch<td>   ");
        sBuildHTML.append("<td class=\"anime2drop\">drop<td>   ");
        sBuildHTML.append("<td class=\"anime2notonlist\">not_on_list<td>   ");
        sBuildHTML.append("<td class=\"anime2onhold\"><b style=\"background-color: #ffd700; color: black;\">best_in_a_way!</b><td>   ");
        sBuildHTML.append("</tr></table>\n");
        return sBuildHTML.toString();
    }
}

package hu.sasukeskapa;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class Main {
    public static String untilAPIaccessFileLocation = "";
    public static String htmlPath = "";
    public static boolean verbose = false;
    public static String devHTMLlocation = "/home/sasukeskapa/Dropbox/progs_linux_etc/www/animecompare.html";
    public static String IntelliJWorkspace = "/home/sasukeskapa/workspace/workspace_IntelliJ/OtakuCornerAnimeListCompare";
    public static String IntelliJjarFolder = "/home/sasukeskapa/workspace/workspace_IntelliJ/OtakuCornerAnimeListCompare/out/artifacts/OtakuCornerAnimeListCompare_jar";

    public static void main(String[] args) {
        untilAPIaccessFileLocation = System.getProperty("user.dir") + File.separator;
        htmlPath = ".." + File.separator + "www" + File.separator + "otakucornercompare.html";
        ArrayList<String> userNames = new ArrayList<>();
        // cd workspace_IntelliJ/OtakuCornerAnimeListCompare/out/artifacts/OtakuCornerAnimeListCompare_jar/
        // java -jar OtakuCornerAnimeListCompare.jar -u 'sasukeskapa;adizz00;1k0;Yui-chan12;adam112' -f '/home/sasukeskapa/workspace_IntelliJ/OtakuCornerAnimeListCompare/out/artifacts/OtakuCornerAnimeListCompare_jar/'
        System.out.println("Example for parameter usage:");
        System.out.println("java -jar OtakuCornerAnimeListCompare.jar -v -u \'adizz00;1k0;adam112\' -f \'/home/sasukeskapa/\' -o \'/home/sasukeskapa/otakucornercompare.html\'");
        System.out.println("-o \'HTML path\'                 for custom HTML file.");
        System.out.println("The default HTML path: \'.." + File.separator + "www" + File.separator + "otakucornercompare.html\'");
        System.out.println("-f \'workDirectoryPath\'                 for custom work directory.");
        System.out.println("The program will add \'data_source_txts" + File.separator + "\' to the \'workDirectoryPath\' when it uses it's input files :)");
        System.out.println("The default work directory is the directory the program was launched from.");
        System.out.println("-v                 for verbose operation.");
        System.out.println("-u \'usernames\'                 for custom usernames.");
        System.out.println("-h or --help or -help                 for help information only.");

        System.out.println("\nLength of args: " + args.length);
        for (int i = 0; i < args.length; i++) {
            System.out.println(i + 1 + "th argument: " + args[i]);
        }
        for (int i = 0; i < args.length; i++) {
            if ("-u".equals(args[i])) {
                String temp = args[i + 1].replace("\'", "");
                String tempArray[] = temp.split(";");
                userNames = new ArrayList<>(Arrays.asList(tempArray));
                System.out.println("Custom user list inputted: " + userNames);
            } else if ("-f".equals(args[i])) {
                untilAPIaccessFileLocation = args[i + 1].replace("\'", "");
                System.out.println("File input chosen in location: \'" + untilAPIaccessFileLocation + "\'");
            } else if ("-o".equals(args[i])) {
                htmlPath = args[i + 1].replace("\'", "");
                System.out.println("Chosen HTML path: \'" + untilAPIaccessFileLocation + "\'");
            } else if ("-v".equals(args[i])) {
                verbose = true;
                System.out.println("Verbose operation activated.");
            } else if ("-h".equals(args[i]) || "--help".equals(args[i]) || "-help".equals(args[i])) {
                System.out.println("Help is printed as requested.");
                return;
            }
        }
        if (userNames.size() == 0) {
            userNames.add("sasukeskapa");
            userNames.add("Yui-chan12");
            //userNames.add("1k0");
            userNames.add("adizz00");
            userNames.add("adam112");
        }
        System.out.println("usernames: " + userNames + "\n");

        //init db and stuff
        DataBase db = new DataBase();

        //Multi Thread version
        ArrayList<Thread> threadList = new ArrayList<>();
        for (String userName : userNames) {
            threadList.add(new Thread(new DataBaseBuilder(db, untilAPIaccessFileLocation, userName, verbose)));
            threadList.get(threadList.size() - 1).start();
        }
        for (Thread t : threadList) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (db.isFresh()) {
            System.out.println("\n");

            //HTML output / insertion Thread version
            class HTMLthread implements Runnable {
                private DataBase db;

                public HTMLthread(DataBase db) {
                    this.db = db;
                }

                @Override
                public void run() {
                    //HTML table output
                    System.out.println("HTML output generation started.");
                    String outputHTML = SiteGenerator.getHTMLTable(db);
                    try {
                        PrintWriter printer = new PrintWriter(new File(Main.untilAPIaccessFileLocation + "HTMLoutput.txt"));
                        printer.print(outputHTML);
                        printer.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    if (Main.verbose)
                        for (String line : outputHTML.split(System.getProperty("line.separator")))
                            System.out.println(line);
                    System.out.println("HTML output generated.");

                    //HTML insertion
                    System.out.println("\nHTML insertion started.");
                    if (Main.IntelliJWorkspace.equals(System.getProperty("user.dir")) || Main.IntelliJjarFolder.equals(System.getProperty("user.dir"))) {
                        Main.htmlPath = Main.devHTMLlocation;
                    }
                    HTMLinserter.HTMLinsertion(outputHTML, Main.htmlPath);
                    System.out.println("HTML inserted.");
                }
            }
            Thread htmlTh = new Thread(new HTMLthread(db));
            htmlTh.start();

            //CLI output Thread version
            class CLIoutputThread implements Runnable {
                private DataBase db;

                public CLIoutputThread(DataBase db) {
                    this.db = db;
                }

                @Override
                public void run() {
                    System.out.println("\nCLI output generation started.\n");
                    String outputCLI = SiteGenerator.getCLITable(db);
                    try {
                        PrintWriter printer = new PrintWriter(new File(Main.untilAPIaccessFileLocation + "CLIoutput.txt"));
                        printer.print(outputCLI);
                        printer.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    if (Main.verbose)
                        for (String line : outputCLI.split(System.getProperty("line.separator")))
                            System.out.println(line);
                    System.out.println("\nCLI output generated.\n");
                }
            }
            Thread cliTh = new Thread(new CLIoutputThread(db));
            cliTh.start();

            //BrokenDate anime list
            class BrokenDateAnimeThread implements Runnable {
                private DataBase db;

                public BrokenDateAnimeThread(DataBase db) {
                    this.db = db;
                }

                @Override
                public void run() {
                    System.out.println("BrokenDate Anime list:\n" + db.getBrokenDateAnime());
                }
            }
            Thread brokenDateTh = new Thread(new BrokenDateAnimeThread(db));
            brokenDateTh.start();

            //Wait for the Threads to finish!
            try {
                brokenDateTh.join();
                cliTh.join();
                htmlTh.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("\nThe program done it's job, time to wait for the next run :)");
        } else {
            try {
                PrintWriter printer = new PrintWriter(new File(".." + File.separator + "otakucornercompare_update_FAILURE.txt"));
                printer.println(new Date().toString() + " --- Failure, something went wrong in the DataGet part of the program.");
                printer.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}

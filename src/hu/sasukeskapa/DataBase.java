package hu.sasukeskapa;

import java.time.LocalDate;
import java.util.*;

/**
 * Created by sasukeskapa on 2015.06.23. yay
 */
public class DataBase {
    private TreeSet<String> allAnime;
    private TreeSet<String> anyoneWatches;
    private TreeSet<String> anyoneWatchesCurrent;
    private TreeSet<String> anyoneOnHold;
    private TreeSet<String> anyonePlanToWatch;
    private TreeSet<String> anyoneDrop;
    private TreeSet<String> anyoneCompleted;
    private TreeSet<String> everyoneHas;        //only accessed by one Thread, no need to synchronize it!!!
    private TreeSet<String> brokenDateAnime;
    private ArrayList<PersonsData> personsDatas;
    private SortedSet<String> addAllAnime;
    private SortedSet<String> addAnyoneWatches;
    private SortedSet<String> addAnyoneWatchesCurrent;
    private SortedSet<String> addAnyoneOnHold;
    private SortedSet<String> addAnyonePlanToWatch;
    private SortedSet<String> addAnyoneDrop;
    private SortedSet<String> addAnyoneCompleted;
    private SortedSet<String> addBrokenDateAnime;
    private List<PersonsData> addPersonsDatas;

    public DataBase() {
        allAnime = new TreeSet<>();
        anyoneWatches = new TreeSet<>();
        anyoneWatchesCurrent = new TreeSet<>();
        anyoneOnHold = new TreeSet<>();
        anyonePlanToWatch = new TreeSet<>();
        anyoneDrop = new TreeSet<>();
        anyoneCompleted = new TreeSet<>();
        everyoneHas = new TreeSet<>();
        brokenDateAnime = new TreeSet<>();
        personsDatas = new ArrayList<>();
        addAllAnime = Collections.synchronizedSortedSet(allAnime);
        addAnyoneWatches = Collections.synchronizedSortedSet(anyoneWatches);
        addAnyoneWatchesCurrent = Collections.synchronizedSortedSet(anyoneWatchesCurrent);
        addAnyoneOnHold = Collections.synchronizedSortedSet(anyoneOnHold);
        addAnyonePlanToWatch = Collections.synchronizedSortedSet(anyonePlanToWatch);
        addAnyoneDrop = Collections.synchronizedSortedSet(anyoneDrop);
        addAnyoneCompleted = Collections.synchronizedSortedSet(anyoneCompleted);
        addBrokenDateAnime = Collections.synchronizedSortedSet(brokenDateAnime);
        addPersonsDatas = Collections.synchronizedList(personsDatas);
    }

    public void addToAllAnime(String anime) {
        addAllAnime.add(anime);
    }

    public void addToAnyoneWatches(String anime) {
        addAnyoneWatches.add(anime);
    }

    public void addToAnyoneWatchesCurrent(String anime) {
        addAnyoneWatchesCurrent.add(anime);
    }

    public void addToAnyoneOnHold(String anime) {
        addAnyoneOnHold.add(anime);
    }

    public void addToAnyonePlanToWatch(String anime) {
        addAnyonePlanToWatch.add(anime);
    }

    public void addToAnyoneDrop(String anime) {
        addAnyoneDrop.add(anime);
    }

    public void addToAnyoneCompleted(String anime) {
        addAnyoneCompleted.add(anime);
    }

    public void addToPersonsDatas(PersonsData personsData) {
        for (int i = 0; i < personsData.getCurrentlyWatching().size(); i++) {
            AnimeData tempAnimeData = personsData.getCurrentlyWatching().get(i);
            addToAllAnime(tempAnimeData.getTitle());
            addToAnyoneWatches(tempAnimeData.getTitle());
            //temp dates to deal with unset and invalid dates
            LocalDate tempEnds, tempStarts;
            tempEnds = LocalDate.now();
            tempStarts = LocalDate.now();
            //try to parse the dates and log the failures
            try {
                tempEnds = LocalDate.parse(tempAnimeData.getEndDate());
                tempStarts = LocalDate.parse(tempAnimeData.getStartDate());
            } catch (Exception e) {
                String tempStart, tempEnd;
                tempStart = tempAnimeData.getStartDate();
                tempEnd = tempAnimeData.getEndDate();
                if ("".equals(tempStart)) {
                    tempStart = "empty";
                }
                if ("".equals(tempEnd)) {
                    tempEnd = "empty";
                }
                addBrokenDateAnime.add(tempAnimeData.getTitle() + " 's dates are invalid: " + tempStart + " " + tempEnd);
            }
            //check if it airs now and add it to the currently airing list
            if (!LocalDate.now().isAfter(tempEnds) && !LocalDate.now().isBefore(tempStarts)) {
                addToAnyoneWatchesCurrent(tempAnimeData.getTitle());
            }
        }
        for (int i = 0; i < personsData.getOnHold().size(); i++) {
            AnimeData tempAnimeData = personsData.getOnHold().get(i);
            addToAllAnime(tempAnimeData.getTitle());
            addToAnyoneOnHold(tempAnimeData.getTitle());
        }
        for (int i = 0; i < personsData.getPlanToWatch().size(); i++) {
            AnimeData tempAnimeData = personsData.getPlanToWatch().get(i);
            addToAllAnime(tempAnimeData.getTitle());
            addToAnyonePlanToWatch(tempAnimeData.getTitle());
        }
        for (int i = 0; i < personsData.getDropped().size(); i++) {
            AnimeData tempAnimeData = personsData.getDropped().get(i);
            addToAllAnime(tempAnimeData.getTitle());
            addToAnyoneDrop(tempAnimeData.getTitle());
        }
        for (int i = 0; i < personsData.getCompleted().size(); i++) {
            AnimeData tempAnimeData = personsData.getCompleted().get(i);
            addToAllAnime(tempAnimeData.getTitle());
            addToAnyoneCompleted(tempAnimeData.getTitle());
        }
        addPersonsDatas.add(personsData);
    }

    private void createEveryoneHas() {
        for (String currentAnimeTitle : allAnime) {
            boolean someoneDoesNotHasThis = false;
            for (int j = 0; j < personsDatas.size(); j++) {
                if (personsDatas.get(j).getAnime(currentAnimeTitle) == null)
                    someoneDoesNotHasThis = true;
            }
            if (!someoneDoesNotHasThis)
                everyoneHas.add(currentAnimeTitle);
        }
    }

    public TreeSet<String> getEveryoneHas() {
        synchronized (this) {
            if (everyoneHas.size() == 0) {
                createEveryoneHas();
            }
        }
        return everyoneHas;
    }

    public TreeSet<String> getAnyoneWatches() {
        return anyoneWatches;
    }

    public TreeSet<String> getAnyoneWatchesCurrent() {
        return anyoneWatchesCurrent;
    }

    public TreeSet<String> getAnyoneOnHold() {
        return anyoneOnHold;
    }

    public TreeSet<String> getAnyonePlanToWatch() {
        return anyonePlanToWatch;
    }

    public TreeSet<String> getAnyoneCompleted() {
        return anyoneCompleted;
    }

    public TreeSet<String> getAnyoneDrop() {
        return anyoneDrop;
    }

    public ArrayList<PersonsData> getPersonsDatas() {
        return personsDatas;
    }

    public int peopleCount() {
        return personsDatas.size();
    }

    public TreeSet<String> getUserNameSet() {
        TreeSet<String> names = new TreeSet<>();
        for (int i = 0; i < peopleCount(); i++) {
            names.add(personsDatas.get(i).getName());
        }
        return names;
    }

    public String getBrokenDateAnime() {
        StringBuilder sb = new StringBuilder();
        for (String line : brokenDateAnime) {
            sb.append(line + "\n");
        }
        sb.deleteCharAt(sb.length() - 1);     //toString appends a \n, so the last one is unnecessary
        return sb.toString();
    }

    public boolean isFresh() {
        //boolean fresh = true;
        //for (int i = 0; i < personsDatas.size(); i++) {
        //    if (!personsDatas.get(i).isFresh())
        //        fresh = false;
        //}
        //return fresh;
        boolean fresh = false;      //changed the behavior to be true even if only one list gets updated too
        for (int i = 0; i < personsDatas.size(); i++) {
            if (personsDatas.get(i).isFresh())
                fresh = true;
        }
        return fresh;
    }
}

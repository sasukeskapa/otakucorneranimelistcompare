package hu.sasukeskapa;

/**
 * Created by sasukeskapa on 2015.06.23. yay
 */
public class AnimeData {
    private String title;
    private String link;
    private String score;
    private String tag;
    //Watching: 1   Completed: 2    On-hold: 3  Dropped: 4  Plan to Watch: 6
    private String status;
    private String endDate;
    private String startDate;


    public AnimeData() {
        this(null, null, null, null, null, null, null);
    }

    public AnimeData(String title, String link, String score, String tag, String status, String endDate, String startDate) {
        this.title = title;
        this.link = link;
        this.score = score;
        this.tag = tag;
        this.status = status;
        this.endDate = endDate;
        this.startDate = startDate;
    }

    @Override
    public String toString() {
        return "AnimeData{" +
                "title='" + title + '\'' +
                ", link='" + link + '\'' +
                ", score='" + score + '\'' +
                ", tag='" + tag + '\'' +
                ", status=" + status +
                '}';
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
}
